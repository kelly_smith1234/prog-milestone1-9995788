﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var ab = new Dictionary<string, int>();
            var days = new List<Tuple<string>>();

            ab.Add("January", 31);
            ab.Add("February", 28);
            ab.Add("March", 31);
            ab.Add("April", 30);
            ab.Add("May", 31);
            ab.Add("June", 30);
            ab.Add("July", 31);
            ab.Add("August", 31);
            ab.Add("September", 30);
            ab.Add("October", 31);
            ab.Add("November", 30);
            ab.Add("December", 31);

            foreach (var x in ab)
            {
                if (x.Value == 31)
                {
                    days.Add(Tuple.Create(x.Key));
                }
            }
            Console.WriteLine("These are the months that contains 31 days in the year");
            foreach (var y in days)
            {
                Console.WriteLine(y);
            }


        }
    }
}
