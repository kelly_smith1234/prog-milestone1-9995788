﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please give me the time in 24hr format i.e. 23");
            var Time = int.Parse(Console.ReadLine());

            if (Time < 12)
            {
                Console.WriteLine(Time + "am");

            }
            else if (Time == 12)
            {
                Console.WriteLine(Time + "pm");

            }
            else if (Time > 12)
            {
                Time = Time - 12;
                Console.WriteLine(Time + "pm");

            }
            else if (Time == 24)
            {
                Time = Time - 12;
                Console.WriteLine(Time + "am");
            }


        }
    }
}
