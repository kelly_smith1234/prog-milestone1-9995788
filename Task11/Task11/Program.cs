﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            //making an int variable for the year input
            int year = 0;

            //asking user for the input year to be read
            Console.WriteLine("Is your input year a leap year?");
            year = int.Parse(Console.ReadLine());

            //using math problems to sort out the leap years from the non leap years
            if (year % 400 == 0)
            {
                Console.WriteLine("Yes!!");
            }
            else if (year % 100 == 0)
            {
                Console.WriteLine("No!");
            }
            else if (year % 4 == 0) 
            {
                Console.WriteLine("Yes!!");
            }
            else
            {
                Console.WriteLine("No!");
            }


        }
    }
}
