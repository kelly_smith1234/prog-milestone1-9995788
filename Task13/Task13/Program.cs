﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Here I set out all my variables before writing my cosde
            double item1 = 0;
            double item2 = 0;
            double item3 = 0;
            double total = 0;
            double GST = 0;

            //I do a write line command to ask the customer the price of their items
            Console.WriteLine("Hi there! Welcome to the store! What is the cost of your first item?");
            item1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Thank you! What about the second item?");
            item2 = double.Parse(Console.ReadLine());

            Console.WriteLine("What about the third item?");
            item3 = double.Parse(Console.ReadLine());

            //This adds them all so that I can get a total
            total = item1 + item2 + item3;
            GST = total * 0.15;

            Console.WriteLine("Your total comes to $" + (total + GST));
        }
    }
}
