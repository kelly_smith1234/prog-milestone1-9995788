﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task17
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new List<Tuple<string, int>>();
            people.Add(Tuple.Create("John", 31));
            people.Add(Tuple.Create("Jane", 25));
            people.Add(Tuple.Create("Sally", 46));

            foreach (var x in people)
            {
                Console.WriteLine("this is " + x.Item1 + " and they are " + x.Item2 + " years old");
            }

        }
    }
}
