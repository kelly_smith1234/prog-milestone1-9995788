﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task18
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new List<Tuple<string, string, int>>();
            people.Add(Tuple.Create("John", "August", 18));
            people.Add(Tuple.Create("Jane", "February", 25));
            people.Add(Tuple.Create("Sally", "December", 15));

            foreach (var x in people)
            {
                Console.WriteLine("this is " + x.Item1 + " and their birtday is " + x.Item2 + " " + x.Item3);
            }

        }
    }
}
