﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task19
{
    class Program
    {
        static void Main(string[] args)
        {

            var list = new int[7] { 34, 45, 21, 44, 67, 88, 86 };
            var lists = new List<Tuple<int>>();

            foreach (var x in list)
            {
                if (x % 2 == 0)
                {
                    lists.Add(Tuple.Create(x));
                }

               
            }

            Console.WriteLine("These are even numbers from our list");
            foreach (var y in lists)
            {

                Console.WriteLine(y);
            }

        }

    }
    
}
