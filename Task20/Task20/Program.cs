﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task20
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new int[7] { 33, 45, 21, 44, 67, 87, 86 };
            var lists = new List<Tuple<int>>();

            foreach (var x in list)
            {
                if (x % 2 != 0)
                {
                    lists.Add(Tuple.Create(x));
                }


            }

            Console.WriteLine("These are odd numbers from our list");
            foreach (var y in lists)
            {

                Console.WriteLine(y);
            }
        }
    }
}
