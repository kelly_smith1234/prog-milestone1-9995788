﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task23
{
    class Program
    {
        static void Main(string[] args)
        {
            var ab = new Dictionary<string, int>();
            var days = new List<Tuple<string>>();

            ab.Add("Monday", 1);
            ab.Add("Tuesday", 2);
            ab.Add("Wednesday", 3);
            ab.Add("Thursday", 4);
            ab.Add("Friday", 5);
            ab.Add("Saturday", 6);
            ab.Add("Sunday", 7);

            foreach (var x in ab)
            {
                if (x.Value <= 5)
                {
                    Console.WriteLine(x.Key + " is a weekday");
                }
                else
                {
                    Console.WriteLine(x.Key + " is a weekend");
                }
            }

        }
    }
}
