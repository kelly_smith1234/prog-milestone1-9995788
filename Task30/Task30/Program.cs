﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task30
{
    class Program
    {
        static void Main(string[] args)
        {
            int intnum1 = 0;
            int intnum2 = 0;
            var strnum1 = "";
            var strnum2 = "";

            Console.WriteLine("Hi there! I require two numbers from you");
            Console.WriteLine("Number one?");
            strnum1 = Console.ReadLine();
            intnum1 = int.Parse(strnum1);

            Console.WriteLine("Number two");
            strnum2 = Console.ReadLine();
            intnum2 = int.Parse(strnum2);

            Console.WriteLine("These numbers added as strings is equal to " + (strnum1 + strnum2));
            Console.WriteLine("These numbers added as integers is equal to " + (intnum1 + intnum2));





        }
    }
}
