﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task31
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;

            Console.WriteLine("Please input a number to see if it is divisible by 3 and 4");
            num = int.Parse(Console.ReadLine());

            if (num % 3 == 0)
            {
                Console.WriteLine("This number is divisible by 3");
                if (num % 4 == 0)
                {
                    Console.WriteLine("It is also divisible by 4");
                }
                else if (num % 4 != 0)
                {
                    Console.WriteLine("But it is not divisible by 4");
                }


            }

            else if (num % 3 != 0)
            {
                Console.WriteLine("This number is not divisble by 3");
                if (num % 4 == 0)
                {
                    Console.WriteLine("but it is divisible by 4");
                }
                else if (num % 4 != 0)
                {
                    Console.WriteLine("It also is not divisible by 4");

                }

            }
        }
    }
}
