﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task34
{
    class Program
    {
        static void Main(string[] args)
        {
            var workdays = 5;
            var weeks = 0;

            Console.WriteLine("Please input amount of weeks to find out how many working days");
            weeks = int.Parse(Console.ReadLine());

            var amount = weeks * workdays;

            Console.WriteLine("The amount of work days in " + weeks + " weeks is " + amount);

        }
    }
}
